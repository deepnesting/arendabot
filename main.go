package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/rs/xid"
	"github.com/zhuharev/qiwi"
	"github.com/zhuharev/tamework"
)

const (
	version = "0.0.11"
	// GlobalSalt чтобы киви не смогли сопоставить id
	GlobalSalt = 38947982
)

var (
	StartTime = time.Now().Add(-3 * time.Minute)

	processedTxs = map[string]struct{}{}
)

// Usecase some text
type Usecase interface {
	CreateRegRequest(userID int, typ, text string) error
	GetAdminUsers() ([]int, error)
}

type Stater interface {
	SetState(userID int, state State, payloads ...interface{}) error
	GetState(userID int) (State, error)
	GetStateWithPayload(userID int, trarget interface{}) (State, error)
	ResetState(userID int) error
}

type RegRequest struct {
	ID      int
	Type    UserType
	Message string
}

type UserType int

const (
	UserGuest = iota
	UserAgent
	UserFleet
	UserAdmin
)

type ucImp struct {
	b *tamework.Tamework
}

func (u *ucImp) GetAdminUsers() ([]int, error) {
	return []int{102710272}, nil //122882122
}

type State int

const (
	StateUnkonwn State = iota
	AgentWaitRegRequestText
	ServiceWaitRegRequestText
	StateInputAgentName
	StateInputCity
	StateInputPhone
	StateInputFleetName
	StateInputPayoutAmount
	StateInputPayoutAccount
	StateInputCarData
	StateInputDriverData
	StateUploadDriverDocuments
	StateAddOfferChooseCity
	StateAddOfferChooseFleet
	StateAddOfferSendToFleet
	StateSetCarCount
)

func main() {
	b, err := tamework.New(os.Getenv("TELEGRAM_TOKEN"))
	err = b.Send("Перезагрузка! "+version, tamework.ToChat(102710272))
	if err != nil {
		panic(err)
	}

	app := NewApp(b)
	app.registreHandlers()

	if err != nil {
		log.Fatal(err)
		return
	}

	app.Start()
}

type App struct {
	b *tamework.Tamework

	client *Client

	stater Stater
}

func NewApp(b *tamework.Tamework) *App {
	log.Println("init app with base url", os.Getenv("API_BASE_URL"))
	return &App{
		b:      b,
		stater: NewMemStateStore(),
		client: New(os.Getenv("API_BASE_URL")),
	}
}

func (a *App) Start() {
	go a.runBalanceSyncer()
	go a.runNotifier()
	a.b.Run()
}

// NewMemStateStore init new memory state
func NewMemStateStore() Stater {
	return &MemStateStore{
		state:        make(map[int]State),
		statePayload: make(map[int]interface{}),
	}
}

// MemStateStore implement stater, store state inmemory
type MemStateStore struct {
	state        map[int]State
	statePayload map[int]interface{}
	stateMu      sync.RWMutex
}

var _ Stater = (*MemStateStore)(nil)

// SetState store state
func (s *MemStateStore) SetState(userID int, state State, payloads ...interface{}) error {
	s.stateMu.Lock()
	s.state[userID] = state
	if len(payloads) > 0 {
		s.statePayload[userID] = payloads[0]
	}
	s.stateMu.Unlock()
	return nil
}

// ResetState reset user state
func (s *MemStateStore) ResetState(userID int) error {
	s.SetState(userID, StateUnkonwn)
	return nil
}

func (s *MemStateStore) GetState(userID int) (State, error) {
	s.stateMu.RLock()
	defer s.stateMu.RUnlock()

	r := s.state[userID]
	return r, nil
}

// GetState get current user state
func (s *MemStateStore) GetStateWithPayload(userID int, target interface{}) (State, error) {
	var r State
	var p interface{}
	var (
		hasState   bool
		hasPayload bool
	)
	s.stateMu.RLock()
	r, hasState = s.state[userID]
	p, hasPayload = s.statePayload[userID]
	s.stateMu.RUnlock()

	if !hasPayload || !hasState {
		return 0, fmt.Errorf("state does not exists")
	}

	reflect.ValueOf(target).Elem().Set(reflect.ValueOf(p).Elem())

	return r, nil
}

func (a *App) authMiddleware(c *tamework.Context) {
	ctx := context.TODO()
	user, err := a.client.UsersByID(ctx, int(c.UserID))
	state, _ := a.stater.GetState(int(c.UserID))
	if (user.Role == 0 || user.Role == UserRoleGuest) && (c.Text != buttonIAgent && c.Text != buttonIService && state != ServiceWaitRegRequestText && state != AgentWaitRegRequestText) {
		log.Println("user not found", user, err)
		a.StartHandler(c)
		c.Exit()
		return
	}
	log.Printf("User found: %d", user.ID)

	// update username and name if needed
	if user.Username != c.Update().Username() {
		user.Username = c.Update().Username()
		a.client.UsersUpdate(context.TODO(), user)
	}
}

func (a *App) registreHandlers() {

	a.b.NotFound = func(c *tamework.Context) {
		state, _ := a.stater.GetState(int(c.UserID))
		switch state {
		case StateInputPayoutAccount:
			a.handleInputPayoutAccount(c)
		case StateInputPayoutAmount:
			a.handleInputPayoutAmount(c)
		// case StateUnkonwn:
		// 	a.StartHandler(c)
		case ServiceWaitRegRequestText, AgentWaitRegRequestText:
			a.RegRequestHandler(c)
		case StateInputAgentName:
			var user User
			a.stater.GetStateWithPayload(int(c.UserID), &user)
			a.HandleAgentName(&user, c)
		case StateInputFleetName:
			var user User
			a.stater.GetStateWithPayload(int(c.UserID), &user)
			a.HandleFleetName(&user, c)
		case StateInputCity:
			var user User
			a.stater.GetStateWithPayload(int(c.UserID), &user)
			a.HandleAgentCity(&user, c)
		case StateInputPhone:
			var user User
			a.stater.GetStateWithPayload(int(c.UserID), &user)
			a.HandleAgentPhone(&user, c)
		case StateInputCarData:
			a.HandleCreateCar(c)
		case StateInputDriverData:
			a.HandleCreateDriver(c)
		case StateUploadDriverDocuments:
			if c.Text == buttonSubmit {
				a.saveDriverDocs(c)
				return
			}
			a.HandleUploadDriverDocuments(c)
		case StateAddOfferChooseFleet:
			a.HandleChooseOffer(c)
		case StateAddOfferSendToFleet:
			a.HandleCreateOffer(c)
		case StateAddOfferChooseCity:
			a.HandleAddOfferCreate(c)
		case StateSetCarCount:
			a.SetCarCount(c)
		default:
			a.StartHandler(c)
		}
	}

	a.b.Use(a.authMiddleware)

	a.b.Text("/start", a.StartHandler)

	a.b.Text(buttonCancel, a.StartHandler)

	// посылает сообщение о том что нужно писать в тексте заявки
	a.b.Text(buttonIAgent, a.IAgentHandler)
	a.b.Text(buttonIService, a.IServiceHandler)
	a.b.Text(buttonBalance, a.balanceHandler)
	a.b.Text(buttonDrivers, a.driverHandler)
	a.b.Text(buttonOffers, a.offersHandler)
	a.b.Text(buttonCars, a.carsHandler)
	a.b.Text(buttonFleets, a.fleetsHandler)

	a.b.Text("noop", func() {})

	a.b.CallbackQuery(buttonPayout, a.handlePayoutFirstStep)
	a.b.CallbackQuery("createCar", a.handleCreateCarRequest)
	a.b.CallbackQuery("addDriver", a.handleAddDriverRequest)

	a.b.Prefix("rejectAgent:", a.HandleReject)
	a.b.Prefix("rejectFleet:", a.HandleReject)

	a.b.Prefix("submitAgent:", a.HandleSubmitAgent)
	a.b.Prefix("submitFleet:", a.HandleSubmitFleet)

	a.b.Prefix("addDocument:", a.HandleAddDocuments)
	a.b.Prefix("showDocuments:", a.HandleShowDocuments)

	a.b.Prefix("addOffer:", a.HandleAddOffer)
	a.b.Prefix("showDriverOffers:", a.ShowDriverOffers)
	a.b.Prefix("rejectOffer:", a.RejectOffer)
	a.b.Prefix("allowDriver:", a.AllowDriver)
	a.b.Prefix("closeOffer:", a.CloseOffer)
	a.b.Prefix("closeShift:", a.CloseShift)
	a.b.Prefix("rateShift:", a.RateShift)

	a.b.Prefix("changeFreeCar:", a.CarCount)

	a.b.Prefix("/offer", a.HandlerOffer)
	a.b.Prefix("/car", a.HandleCar)
	a.b.Prefix("/driver", a.HandleDriver)
	a.b.Prefix("/shift", a.HandleShift)

	a.b.Prefix("inputPayoutAccount", a.handlePayoutFirstStep)

	//a.b.NotFound(tb.OnText, a.AllHandler)
	//a.b.Text(tb.OnCallback, a.AllCallbackHandlers)
}

const (
	buttonCancel = "Отмена"
	buttonSubmit = "Сохранить"

	buttonIAgent   = "Я агент"
	buttonIService = "Я парк"

	buttonBalance = "Баланс"
	buttonRefill  = "Пополнить"
	buttonPayout  = "Вывести"

	buttonDrivers = "Водители"
	buttonCars    = "Машины"
	buttonFleets  = "Парки"
	buttonOffers  = "Заявки"

	changeFreeCar = "changeFreeCar"
)

var (
	_fleetMainKeyboard = tamework.NewKeyboard(nil).AddReplyButton(buttonOffers).AddReplyButton(buttonCars).AddReplyButton("").AddReplyButton(buttonBalance)
	_agentMainKeyboard = tamework.NewKeyboard(nil).AddReplyButton(buttonDrivers).AddReplyButton(buttonFleets).AddReplyButton("").AddReplyButton(buttonBalance)
)

func (a *App) qiwiCredentials() (account string, token string, err error) {
	pss, err := a.client.PaymentsList(context.TODO())
	if err != nil {
		return
	}
	if len(pss) == 0 {
		err = fmt.Errorf("no systems returned by api")
		return
	}

	arr := strings.Split(pss[0].Setting, ":")
	if len(arr) != 2 {
		err = fmt.Errorf("bad wallet setting: %s", pss[0].Setting)
		return
	}
	return arr[0], arr[1], nil
}

func (a *App) handleInputPayoutAmount(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}
	a.stater.ResetState(user.ID)

	amount, err := strconv.Atoi(c.Text)
	if err != nil {
		c.Send("Неправильная сумма!")
		return
	}

	balance, err := a.getUserBalance(context.TODO(), user.ID)

	if err != nil {
		c.Send("Ошибка")
		return
	}

	if amount > balance {
		c.Send("Не могу ывести больше чем на балансе!")
		return
	}

	_, err = a.client.WalletCreateTransaction(context.TODO(), Transaction{
		Amount: -float64(amount),
		Status: TransactionStatusDone,
		UserID: user.ID,
	})
	if err != nil {
		c.Send("ошибка")
		return
	}

	qiwiWallet, qiwiToken, err := a.qiwiCredentials()
	if err != nil {
		log.Printf("err get qiwi creds err=%s", err)
		c.Send("Ошибка!")
		return
	}

	qw := qiwi.New(qiwiToken, qiwi.Wallet(qiwiWallet))
	pr, err := qw.Payments.Payment(context.TODO(), qiwi.QiwiProviderID, float64(amount), "+7"+user.PayoutAccount, "")
	if err != nil {
		c.Send("ошибка платежа: " + err.Error())
		return
	}
	// TODO update tx id
	if pr.ID == "" {
		c.Send("ошибка платежа")
		return
	}
	c.Send("Операция прошла успешно, проверьте баланс!")
	// tODO: save tx
	// t.Logf("ID транзакции: %s", paymentResponse.Transaction.ID)
}

func (a *App) handleInputPayoutAccount(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}
	user.PayoutAccount = c.Text
	if len(c.Text) != 10 {
		c.Markdown("Не верный формат (пример: `9001234567`)!")
		return
	}
	a.stater.ResetState(int(user.ID))
	err := a.client.UsersUpdate(context.TODO(), user)
	if err != nil {
		log.Printf("err update user err=%s", err)
		return
	}
	c.Send("Информация обновлена!")
}

func (a *App) handleCreateCarRequest(c *tamework.Context) {
	c.Answer("")
	c.Markdown("Данные об автомобиле нужно отправлять одним сообщением, каждый пункт начинать с новой строки.\n\nВведите данные об автомобиле в следующем формате:\n\n`марка\nмодель\nгод\nтопливо\nкоробка\nбренд (да/нет)\nцена\nколичество свободных\nкомментарий`\n\n(например:\n`киа\nрио\n2018\nпропан\nавтомат\nда\n1500\n2\nновая!`\n)")
	a.stater.SetState(int(c.UserID), StateInputCarData)
}

func (a *App) handleAddDriverRequest(c *tamework.Context) {
	c.Answer("")
	c.Markdown("Данные о водителе нужно отправлять одним сообщением, каждый пункт начинать с новой строки.\n\nВведите данные о водителе в следующем формате:\n\n`имя\nтелефон`\n\n(например:\n`иван\n99912345678`\n)")
	a.stater.SetState(int(c.UserID), StateInputDriverData)
}

func (a *App) handlePayoutFirstStep(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}
	a.stater.SetState(user.ID, StateInputPayoutAccount)
	c.Send("Введите номер кошелька, куда выводить деньги (десять цифр, без кода страны):")
}

func (a *App) offersHandler(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}

	if user.Role != UserRoleFleet {
		c.Send("Доступ запрещен!")
	}

	offers, err := a.client.OffersByFleetID(context.TODO(), user.ID)
	if len(offers) == 0 {
		c.Send("У вас пока нет заявок")

		if err != nil {
			log.Println(err)
		}
		return
	}

	// filter offer
	// Для парка не нужно отображать отклоненные заявки.
	var filteredOffers []Offer
	for _, o := range offers {
		if !o.IsEnded() {
			filteredOffers = append(filteredOffers, o)
		}
	}

	ctx := context.Background()
	drivers, err := a.client.DriversList(ctx)
	if err != nil {
		handleError(c, err)
		return
	}

	msg, err := renderOffers(filteredOffers, drivers)
	if err != nil {
		log.Println(err)
		c.Send("Ошибка!")
	}

	c.Send(msg)

}

func (a *App) carsHandler(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}

	if user.Role != UserRoleFleet {
		c.Send("Доступ запрещен!")
	}

	c.Keyboard.AddCallbackButton("Добавить машину", "createCar")

	cars, err := a.client.CarsByFleetID(context.TODO(), user.ID)
	if len(cars) == 0 {
		c.Send("Ваш парк без машин, добавьте их!")

		if err != nil {
			log.Println(err)
		}
		return
	}

	msg, err := renderCars(cars)
	if err != nil {
		log.Println(err)
		c.Send("Ошибка!")
	}

	c.Send(msg)

}

func (a *App) fleetsHandler(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}

	if user.Role != UserRoleAgent {
		c.Send("Доступ запрещен!")
	}

	//c.Keyboard.AddCallbackButton("Добавить машину", "createCar")

	fleetUsers, err := a.client.UsersByRole(context.TODO(), int(UserRoleFleet))
	if len(fleetUsers) == 0 {
		c.Send("Администраторы не добавили ни одного парка!")

		if err != nil {
			log.Println(err)
		}
		return
	}

	// проверяем что у парка достаточно денег на балансе
	var fleets []User
	for _, fleet := range fleetUsers {
		balance, err := a.getUserBalance(context.TODO(), fleet.ID)
		if err != nil {
			continue
		}
		if balance < fleet.Fee {
			continue
		}
		fleets = append(fleets, fleet)
	}

	msg, err := renderFleets(fleets)
	if err != nil {
		log.Println(err)
		c.Send("Ошибка!")
	}

	c.Markdown(msg)

}

func (a *App) driverHandler(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	if user.ID == 0 {
		return
	}

	if user.Role == UserRoleFleet {
		//	a.client.Drivers
	} else if user.Role == UserRoleAgent {
		drivers, _ := a.client.DriversByAgentID(context.TODO(), int(c.UserID))
		c.Keyboard.AddCallbackButton("Добавить водителя", "addDriver")
		if len(drivers) == 0 {
			c.Send("Вы не добавили ни одного водителя!")
			return
		}

		//TODO: После выполненных платных смен водитель иcчезает их списка.
		var filteredDrivers []Driver
		for _, driver := range drivers {
			filteredDrivers = append(filteredDrivers, driver)
		}

		str, _ := renderDrivers(filteredDrivers)
		c.Markdown(str)
		return
	}

	c.Send("allo")
}

func (a *App) getUserBalance(ctx context.Context, userID int) (int, error) {
	transactions, err := a.client.WalletUserTransactions(context.TODO(), userID)
	if err != nil {
		return 0, err
	}
	var balance int
	for _, t := range transactions {
		if t.Status == TransactionStatusDone || t.Status == TransactionStatusLock {
			balance += int(t.Amount)
		}
	}
	return balance, nil
}

func (a *App) balanceHandler(c *tamework.Context) {
	balance, err := a.getUserBalance(context.TODO(), int(c.UserID))
	if err != nil {
		c.Send("Ошибка!")
		return
	}

	user, err := a.client.UsersByID(context.TODO(), int(c.UserID))
	if err != nil {
		handleError(c, err)
		return
	}

	qiwiWallet, _, err := a.qiwiCredentials()
	if err != nil {
		log.Printf("err get qiwi creds err=%s", err)
		return
	}
	comment := fmt.Sprintf("%d", c.UserID+GlobalSalt)

	description := fmt.Sprintf("Комментарий пополнения: `%s` (*При пополнении обязателен комментарий, иначе деньги не поступят на ваш счёт!*)", comment)
	if user.Role == UserRoleAgent {
		if user.PayoutAccount == "" {
			description = "*Внимание! Нужно настроить кошелек для вывода средств, иначе деньги не будут начисляться!*"
		} else {
			description = fmt.Sprintf("Начисления будут переводиться на qiwi-кошелек по номеру: %s", user.PayoutAccount)
		}
		c.Keyboard = tamework.NewKeyboard(nil).AddCallbackButton("Настроить qiwi-кошелёк для вывода", "inputPayoutAccount")
	} else if user.Role == UserRoleFleet {
		if qiwiWallet != "" {
			c.Keyboard = tamework.NewKeyboard(nil).
				AddURLButton(buttonRefill, generatePaymentLink(qiwiWallet, 1000, comment))
		}
	}

	text := fmt.Sprintf("Ваш баланс: %d руб.", balance)

	_, err = c.Markdown(fmt.Sprintf("%s\n\n%s", text, description))
	if err != nil {
		handleError(c, err)
	}
}

func (a *App) depositHandler(c *tamework.Context) {
	qiwiWallet, _, err := a.qiwiCredentials()
	if err != nil {
		log.Printf("err get qiwi creds err=%s", err)
		c.Send("Ошибка!")
		return
	}
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	balance, _ := a.getUserBalance(context.TODO(), user.ID)
	comment := fmt.Sprintf("%d", c.UserID+GlobalSalt)
	c.Keyboard = tamework.NewKeyboard(nil).
		AddCallbackButton(buttonPayout).
		AddURLButton(buttonRefill, generatePaymentLink(qiwiWallet, 1000, comment))
	c.Send(fmt.Sprintf("Ваш баланс %d руб.", balance))
}

func (a *App) IsLoggedIn(c *tamework.Context) bool {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	return user.ID != 0
}

func (a *App) IsApprovedUser(c *tamework.Context) bool {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
	return user.ID != 0 && user.Role != 0 && user.Role != UserRoleGuest
}

func (a *App) StartHandler(c *tamework.Context) {
	if a.IsApprovedUser(c) {
		user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))
		if user.Role == UserRoleFleet {
			c.Keyboard = _fleetMainKeyboard
		} else if user.Role == UserRoleAgent {
			c.Keyboard = _agentMainKeyboard
		}
		c.Send(fmt.Sprintf("Добро пожаловать!\n\nВаша роль: %s", roleText[user.Role]))
	} else {
		c.Keyboard = tamework.NewKeyboard(nil).
			AddReplyButton(buttonIAgent).AddReplyButton(buttonIService)
		c.Send("Выберите вашу роль:")
	}
}

func (a *App) IAgentHandler(c *tamework.Context) {
	if strings.HasPrefix(c.Update().Username(), "_") {
		c.Send("Необходимо установить юзернейм")
		return
	}

	ctx := context.TODO()
	user, _ := a.client.UsersByID(ctx, int(c.UserID))
	if user.ID == 0 {
		user.ID = int(c.UserID)
		user.Role = UserRoleGuest
		_, err := a.client.UsersCreate(ctx, user)
		if err != nil {
			id := xid.New().String()
			log.Printf("err create user id=%s err=%s", id, err)
			c.Send(fmt.Sprintf("неизвестная  ошибка: `%s`", id))
		}
	}

	if user.Role.IsActive() {
		a.StartHandler(c)
		return
	}

	c.Send("Напишите текст заявки на регистрацию (имя, фамилия, номер телефона, город) в одном сообщении:")
	a.stater.SetState(int(c.UserID), AgentWaitRegRequestText)
}

func (a *App) IServiceHandler(c *tamework.Context) {
	if strings.HasPrefix(c.Update().Username(), "_") {
		c.Send("Необходимо установить юзернейм")
		return
	}
	ctx := context.TODO()
	user, _ := a.client.UsersByID(ctx, int(c.UserID))
	if user.ID == 0 {
		user.ID = int(c.UserID)
		user.Role = UserRoleGuest
		_, err := a.client.UsersCreate(ctx, user)
		if err != nil {
			id := xid.New().String()
			log.Printf("err create user id=%s err=%s", id, err)
			c.Send(fmt.Sprintf("неизвестная  ошибка: `%s`", id))
			return
		}
	}

	if user.Role.IsActive() {
		a.StartHandler(c)
		return
	}

	c.Send("Напишите текст заявки на регистрацию (название, город, контактный телефон) в одном сообщении:")
	a.stater.SetState(int(c.UserID), ServiceWaitRegRequestText)
}

func (a *App) RegRequestHandler(c *tamework.Context) {
	c.Send("Заявка отправлена администраторам, как только её примут, я пришлю уведомление в этот чат.")
	state, _ := a.stater.GetState(int(c.UserID))
	a.stater.ResetState(int(c.UserID))
	text := "агент"
	if state == ServiceWaitRegRequestText {
		text = "парк"
	}

	adminUsers, err := a.client.UsersByRole(context.TODO(), UserRoleAdmin.Int())
	if err != nil {
		handleError(c, err)
		return
	}
	var submitButton = "submitAgent"
	var rejectButton = "rejectAgent"
	log.Println("create request", text)
	if text == "парк" {
		submitButton = "submitFleet"
		rejectButton = "rejectFleet"
	}

	kb := tamework.NewKeyboard(nil).
		AddCallbackButton("Принять", submitButton+":"+strconv.Itoa(int(c.UserID))).
		AddCallbackButton("Отклонить", rejectButton+":"+strconv.Itoa(int(c.UserID)))

	log.Printf("notify %d admins", len(adminUsers))
	for _, u := range adminUsers {
		err := a.b.Send(fmt.Sprintf("Новая заявка от @%s! Тип: "+text+"\n\n"+c.Text, c.Update().Username()), tamework.ToChat(u.ID), tamework.WithKeyboard(kb))
		if err != nil {
			log.Println(err)
		}
	}
	return

}

func (a *App) HandleReject(c *tamework.Context) {
	c.Answer("")
	log.Printf("reject user=%s", c.Text)
	userID, _ := strconv.Atoi(c.Text)
	c.Send("Заявка отклонена")

	a.b.Send("Ваша заявка отклонена!", tamework.ToChat(userID))
}

// HandleSubmitAgent принимает заявку
func (a *App) HandleSubmitAgent(c *tamework.Context) {
	log.Printf("submit agent text=%s", c.Text)
	c.Answer("")
	userID, _ := strconv.Atoi(c.Text)
	user := &User{
		ID:   userID,
		Role: UserRoleAgent,
	}
	a.stater.SetState(int(c.UserID), StateInputAgentName, user)
	//c.Send("Введите имя Агента")
	c.Keyboard.Reset()
	c.Send("Введите имя Агента")
}

func (a *App) HandlerOffer(c *tamework.Context) {
	user, _ := a.client.UsersByID(context.TODO(), int(c.UserID))

	offer, err := a.client.OffersByID(context.TODO(), strToInt(c.Text))
	if err != nil {
		c.Send(err.Error())
		return
	}

	log.Printf("%+v", offer)

	if offer.FleetID != int(c.UserID) && offer.AgentID != int(c.UserID) {
		c.Send("Не существует")
		return
	}

	fleet, err := a.client.UsersByID(context.TODO(), offer.FleetID)
	if err != nil {
		c.Send(err.Error())
		return
	}

	agent, err := a.client.UsersByID(context.TODO(), offer.AgentID)
	if err != nil {
		c.Send(err.Error())
		return
	}

	driver, err := a.client.DriversByID(context.TODO(), offer.DriverID)
	if err != nil {
		c.Send(err.Error())
		return
	}

	shifts, _ := a.client.ShiftsByOfferID(context.TODO(), offer.ID)

	str, _ := renderOffer(offer, fleet, agent, driver, shifts)

	if user.Role == UserRoleFleet && offer.Status == OfferStatusApproving {
		a.sendImages(offer.FleetID, driver.Images)

		c.Keyboard.AddCallbackButton("Отклонить", "rejectOffer:"+c.Text).AddCallbackButton("Подтвердить", "allowDriver:"+strconv.Itoa(offer.DriverID))
	}
	c.Markdown(str)
}

func (a *App) HandleCar(c *tamework.Context) {
	c.Send("car" + c.Text)
}

func (a *App) HandleShift(c *tamework.Context) {
	shiftID, _ := strconv.Atoi(c.Text)
	shift, _ := a.client.ShiftsByID(context.TODO(), shiftID)
	offer, err := a.client.OffersByID(context.TODO(), shift.OfferID)
	if err != nil {
		c.Send("Ошибка " + err.Error())
		return
	}

	log.Printf("%+v", offer)

	text, _ := renderShift(shift, offer)
	if offer.FleetID == int(c.UserID) && (shift.Status == 0 || shift.Status == ShiftStatusActive) {
		c.Keyboard.AddCallbackButton("Закрыть смену", "closeShift:"+c.Text)
	}
	//c.Keyboard.AddCallbackButton("Отменить заявку", "closeShift:"+c.Text)
	c.Markdown(text)
}

func (a *App) HandleDriver(c *tamework.Context) {
	driver, err := a.client.DriversByID(context.TODO(), strToInt(c.Text))
	if err != nil {
		c.Send("ошибка")
		return
	}
	r, err := renderDriver(driver)
	if err != nil {
		c.Send("ошибка:" + err.Error())
		return
	}
	if len(driver.Images) > 0 {
		c.Keyboard.AddCallbackButton("Показать документы", "showDocuments:"+strconv.Itoa(driver.ID)).AddCallbackButton("")
	}
	c.Keyboard.AddCallbackButton("Загрузить документы", "addDocument:"+strconv.Itoa(driver.ID)).AddCallbackButton("")
	c.Keyboard.AddCallbackButton("Показать заявки по водителю", "showDriverOffers:"+strconv.Itoa(driver.ID)).AddCallbackButton("")
	c.Keyboard.AddCallbackButton("Создать заявку", "addOffer:"+strconv.Itoa(driver.ID))
	c.Markdown(r)
}

func (a *App) sendImages(chatID int, imgs []string) error {
	var images []interface{}
	for _, img := range imgs {
		med := tgbotapi.NewInputMediaPhoto(img)
		images = append(images, med)
	}
	msg := tgbotapi.NewMediaGroup(int64(chatID), images)
	_, err := a.b.Bot().Send(msg)
	if err != nil {
		return err
	}
	return nil
}

func (a *App) HandleShowDocuments(c *tamework.Context) {
	log.Println("show docs")
	driver, err := a.client.DriversByID(context.TODO(), strToInt(c.Text))
	if err != nil {
		c.Send("ошибка")
		return
	}

	err = a.sendImages(int(c.ChatID), driver.Images)
	if err != nil {
		log.Println(err)
		c.Send(err.Error())
		return
	}
	c.Answer("")
}

func (a *App) ShowDriverOffers(c *tamework.Context) {
	offerID := strToInt(c.Text)
	offers, err := a.client.OffersByDriverID(context.TODO(), offerID)
	if err != nil {
		// todo: handle error
		c.Markdown("Заявок нет!")
		return
	}

	ctx := context.Background()
	drivers, err := a.client.DriversList(ctx)
	if err != nil {
		handleError(c, err)
		return
	}

	str, err := renderOffers(offers, drivers)
	if err != nil {
		handleError(c, err)
		return
	}

	_, err = c.Markdown(str)
	if err != nil {
		handleError(c, err)
		return
	}
}

func (a *App) RejectOffer(c *tamework.Context) {
	offerID := strToInt(c.Text)
	offer, err := a.client.OffersByID(context.TODO(), offerID)
	if err != nil {
		c.Send("Ошибка")
		return
	}
	if offer.Status != OfferStatusApproving {
		c.Send("Заявка уже обработана!")
		return
	}
	offer.Status = OfferStatusRejected
	err = a.client.OffersUpdate(context.TODO(), offer)
	if err != nil {
		handleError(c, err)
		return
	}
	a.b.Send(fmt.Sprintf("Заявка %d отклонена парком /offer%d", offerID, offerID), tamework.ToChat(offer.AgentID))
	c.Send("Заявка отклонена!")
}

func (a *App) AllowDriver(c *tamework.Context) {
	driverID := strToInt(c.Text)
	fleetID := int(c.UserID)

	var offer Offer
	offers, _ := a.client.OffersByDriverID(context.TODO(), driverID)
	for _, o := range offers {
		if o.FleetID == fleetID {
			offer = o
		}
	}

	if offer.ID == 0 {
		c.Send("Заявки не существует!")
		//TODO: alert here
		return
	}

	log.Printf("offer=%+v", offer)

	if offer.Status != OfferStatusApproving {
		log.Printf("status=%d statusText=%s", offer.Status, offer.StatusText())
		c.Send("В этом статусе нельзя подтвердить водителя! Статус: " + offer.StatusText())
		return
	}

	offer.Status = OfferStatusApproved
	err := a.client.OffersUpdate(context.TODO(), offer)
	if err != nil {
		c.Send("Ошибка")
		return
	}

	// ensure updated
	offer, err = a.client.OffersByID(context.TODO(), offer.ID)
	if err != nil {
		c.Send("Ошибка")
		return
	}
	if offer.Status == 0 || offer.Status != OfferStatusApproved {
		c.Send("Что-то пошло не так")
		return
	}

	fleet, err := a.client.UsersByID(context.TODO(), offer.FleetID)
	if err != nil {
		c.Send("Ошибка")
		return
	}

	ctx := context.TODO()

	driver, err := a.client.DriversByID(ctx, driverID)
	if err != nil {
		handleError(c, err)
		return
	}

	a.b.Send(
		fmt.Sprintf("Парк '"+fleet.Name+"' одобрил водителя: %s (%s), теперь вы можете отправить водителя в этот парк.",
			driver.Name, driver.Phone,
		),
		tamework.ToChat(offer.AgentID),
		tamework.WithKeyboard(tamework.NewKeyboard(nil).AddCallbackButton("Отправить водителя", "closeOffer:"+strconv.Itoa(offer.ID))),
	)
	c.Send("Водитель одобрен!")
}

// CloseShift закрывает смену, выплачивает
func (a *App) CloseShift(c *tamework.Context) {
	log.Printf("prepare close shift=%s", c.Text)
	for i := 0; i < 5; i++ {
		c.Keyboard.AddCallbackButton(strings.Repeat("⭐️", i+1), "rateShift:"+c.Text+":"+strconv.Itoa(i+1)).AddCallbackButton("")
	}
	c.Send("Оцените водителя")
}

func (a *App) CarCount(c *tamework.Context) {
	ctx := context.Background()
	carID, _ := strconv.Atoi(c.Text)

	car, err := a.client.CarsByID(ctx, carID)
	if err != nil {
		handleError(c, err)
		return
	}
	if car.FleetUserID != int(c.UserID) {
		c.Send("Вы не владелец!")
		return
	}

	a.stater.SetState(int(c.UserID), StateSetCarCount, &car)
	c.Send("Введите актуальное число:")
}

func (a *App) SetCarCount(c *tamework.Context) {
	ctx := context.Background()
	var car Car
	_, err := a.stater.GetStateWithPayload(int(c.UserID), &car)
	if err != nil {
		handleError(c, err)
		return
	}

	car.FreeCount, _ = strconv.Atoi(c.Text)
	err = a.client.CarsUpdate(ctx, car)
	if err != nil {
		handleError(c, err)
		return
	}
	c.Send("Данные обновлены!")
}

//TODO: переместить на сервер в одну транзакцию
// RateShift обновляет рейтинг водителя в смене, закрывает смену и начисляет деньги
func (a *App) RateShift(c *tamework.Context) {
	ctx := context.Background()

	arr := strings.Split(c.Text, ":")
	if len(arr) != 2 {
		return
	}

	shiftID, _ := strconv.Atoi(arr[0])
	rating, _ := strconv.Atoi(arr[1])
	if rating > 5 && rating < 0 {
		return
	}

	log.Printf("rate shift shift=%v rating=%v", shiftID, rating)

	shift, err := a.client.ShiftsByID(context.TODO(), shiftID)
	if err != nil {
		handleError(c, err)
		return
	}

	offer, err := a.client.OffersByID(context.TODO(), shift.OfferID)
	if err != nil {
		handleError(c, err)
		return
	}

	if int(c.UserID) != offer.FleetID {
		c.Send("Закрыть смену может только парк!")
		return
	}

	if shift.Status != 0 && shift.Status != ShiftStatusActive {
		log.Printf("cannot close closed shift status=%v statusText=%s", shift.Status, shift.StatusText())
		c.Send("Смена уже закрыта!")
		return
	}

	fleet, err := a.client.UsersByID(context.TODO(), offer.FleetID)
	if err != nil {
		handleError(c, err)
		return
	}

	// Забираем деньги у парка
	fleetTxID, err := a.client.WalletCreateTransaction(context.TODO(), Transaction{
		Amount: -float64(fleet.Fee),
		UserID: fleet.ID,
		Status: TransactionStatusDone,
	})
	if err != nil {
		handleError(c, err)
		return
	}
	// Начисляем деньги агенту
	agentTxID, err := a.client.WalletCreateTransaction(context.TODO(), Transaction{
		Amount: float64(fleet.AgentReward),
		UserID: offer.AgentID,
		Status: TransactionStatusDone,
	})
	if err != nil {
		handleError(c, err)
		return
	}

	shift.Rating = rating
	shift.Status = ShiftStatusDone
	shift.FleetTransactionTransactionID = fleetTxID
	shift.AgentTransactionTransactionID = agentTxID
	err = a.client.ShiftsUpdate(context.TODO(), shift)
	if err != nil {
		handleError(c, err)
		return
	}

	// пересчитываем рейтинг водителя
	shifts, err := a.client.ShiftsByDriverID(ctx, offer.DriverID)
	if err != nil {
		handleError(c, err)
		return
	}
	var totalRating int
	for _, s := range shifts {
		totalRating += s.Rating
	}

	driver, err := a.client.DriversByID(ctx, offer.DriverID)
	if err != nil {
		handleError(c, err)
		return
	}
	driver.Rating = int(float64(totalRating) / float64(len(shifts)))
	err = a.client.DriversUpdate(ctx, driver)
	if err != nil {
		handleError(c, err)
		return
	}

	balance, err := a.getUserBalance(ctx, offer.AgentID)
	if err != nil {
		handleError(c, err)
		return
	}

	a.b.Send(fmt.Sprintf("Вам начислено %v₽ по заявке %d. Новый баланс: %d. Парк: %s. Водитель: %s (%s). Номер смены: %d/%d.",
		fleet.AgentReward, offer.ID, balance, fleet.Name, driver.Name, driver.Phone, shift.ShiftNumber, fleet.ShiftCount), tamework.ToChat(offer.AgentID),
	)

	shiftNumber := shift.ShiftNumber + 1

	// Если нужно, создаем следующую смену
	if shiftNumber <= fleet.ShiftCount {
		_, err := a.client.ShiftsCreate(context.TODO(), Shift{
			OfferID:     offer.ID,
			CreatedAt:   time.Now(),
			ShiftNumber: shiftNumber,
			Status:      ShiftStatusActive,
		})
		if err != nil {
			handleError(c, err)
			return
		}
	} else {
		// делаем водителя неактивным
		driver.Status = DriverStatusDisabled
		err := a.client.DriversUpdate(ctx, driver)
		if err != nil {
			handleError(c, err)
			return
		}
	}

	c.Keyboard.Reset()
	c.EditText("Спасибо!")

	c.Markdown("Смена закрыта!")
}

// CloseOffer агент может закрыть оффер путем отправки водителя в парк
func (a *App) CloseOffer(c *tamework.Context) {
	offerID := strToInt(c.Text)

	offer, err := a.client.OffersByID(context.TODO(), offerID)
	if err != nil {
		c.Send("Ошибка")
		return
	}

	log.Printf("%+v", offer)

	// TODO: race

	if offer.Status != 0 && offer.Status != OfferStatusApproved {
		c.Send("Заявка уже отправлена: " + offer.StatusText())
		return
	}

	offer.Status = OfferStatusWorking
	err = a.client.OffersUpdate(context.TODO(), offer)
	if err != nil {
		c.Send("Ошибка")
		return
	}

	fleet, err := a.client.UsersByID(context.TODO(), offer.FleetID)
	if err != nil {
		c.Send("Ошибка")
		return
	}

	// создаем смену
	_, err = a.client.ShiftsCreate(context.TODO(), Shift{
		OfferID:     offer.ID,
		CreatedAt:   time.Now(),
		ShiftNumber: 1,
		Status:      ShiftStatusActive,
	})
	if err != nil {
		c.Send("Ошибка")
		return
	}

	agent, err := a.client.UsersByID(context.TODO(), offer.AgentID)
	if err != nil {
		handleError(c, err)
		return
	}

	driver, err := a.client.DriversByID(context.TODO(), offer.DriverID)
	if err != nil {
		handleError(c, err)
		return
	}

	a.sendImages(fleet.ID, driver.Images)

	a.b.Send(fmt.Sprintf("Агент отправит вам водителя в ближайшее время.\nАгент: %s (@%s)\nВодитель: %s (%s)\nВстречайте!",
		agent.Name,
		agent.Username,
		driver.Name,
		driver.Phone,
	),
		tamework.ToChat(fleet.ID),
	)

	c.Answer("Смена создана")
	c.Send("Как только водитель закончит смену, мы начислим бонус на ваш счёт.")
}

func (a *App) HandleAddOffer(c *tamework.Context) {
	driverID := strToInt(c.Text)
	offer := &Offer{DriverID: driverID, AgentID: int(c.UserID), Status: OfferStatusApproving}
	a.stater.SetState(int(c.UserID), StateAddOfferChooseCity, offer)
	cities, err := a.client.UsersCities(context.TODO())
	if err != nil {
		handleError(c, err)
		return
	}
	for _, city := range cities {
		// проверяем что у города есть хотябы один парк
		users, err := a.client.UsersByCityID(context.TODO(), city.ID)
		if err != nil {
			handleError(c, err)
			return
		}
		var cnt int
		for _, u := range users {
			if u.Role != UserRoleFleet {
				continue
			}
			cnt++
		}
		if cnt == 0 {
			continue
		}
		c.Keyboard.AddCallbackButton(city.Title, strconv.Itoa(city.ID)).AddCallbackButton("")
	}
	c.Send("Выберите город:")
}

func (a *App) HandleAddOfferCreate(c *tamework.Context) {
	c.Answer("")
	log.Printf("choosen city=%s", c.Text)
	cityID, err := strconv.Atoi(c.Text)
	if err != nil {
		handleError(c, err)
		return
	}
	var offer Offer
	a.stater.GetStateWithPayload(int(c.UserID), &offer)
	a.stater.SetState(int(c.UserID), StateAddOfferChooseFleet, &offer)
	users, err := a.client.UsersByCityID(context.TODO(), cityID)
	if err != nil {
		handleError(c, err)
		return
	}
	for _, u := range users {
		if u.Role != UserRoleFleet {
			continue
		}
		c.Keyboard.AddCallbackButton(u.Name, strconv.Itoa(u.ID)).AddCallbackButton("")
	}
	c.Send("Выберите парк:")
}

func (a *App) validateFleetBalance(fleetID int) (bool, error) {
	fleet, err := a.client.UsersByID(context.TODO(), fleetID)
	if err != nil {
		return false, err
	}

	fleetBalance, err := a.getUserBalance(context.TODO(), fleet.ID)
	if err != nil {
		return false, err
	}
	// check fleet can pay fee
	if fleetBalance < -299 {
		return false, nil
	}
	return true, nil
}

func (a *App) HandleChooseOffer(c *tamework.Context) {
	c.Answer("")
	var offer Offer
	_, _ = a.stater.GetStateWithPayload(int(c.UserID), &offer)
	fleetID := strToInt(c.Text)
	offer.FleetID = fleetID
	a.stater.SetState(int(c.UserID), StateAddOfferSendToFleet, &offer)

	ctx := context.Background()
	fleet, err := a.client.UsersByID(ctx, fleetID)
	if err != nil {
		handleError(c, err)
		return
	}

	text, err := renderFleet(fleet)
	if err != nil {
		handleError(c, err)
		return
	}

	c.Keyboard.AddCallbackButton("Отправить заявку", strconv.Itoa(fleet.ID))
	c.Send(text)
}

// Заявка в парк
func (a *App) HandleCreateOffer(c *tamework.Context) {
	c.Answer("")
	var offer Offer
	_, _ = a.stater.GetStateWithPayload(int(c.UserID), &offer)
	fleetID := strToInt(c.Text)

	//check fleet exists
	fleetUser, err := a.client.UsersByID(context.TODO(), fleetID)
	if err != nil || fleetUser.ID == 0 || fleetUser.Role != UserRoleFleet {
		c.Send("Парка не существует!")
		return
	}

	// check offer does not exists
	offers, _ := a.client.OffersByDriverID(context.TODO(), offer.DriverID)
	if err != nil {
		//TODO handle not "not found" errors
		return
	}
	for _, o := range offers {
		if o.FleetID == fleetID {
			c.Send("Заявка уже существует /offer" + strconv.Itoa(o.ID))
			return
		}
	}

	agent, _ := a.client.UsersByID(context.TODO(), offer.AgentID)
	driver, _ := a.client.DriversByID(context.TODO(), offer.DriverID)

	if ok, err := a.validateFleetBalance(fleetID); !ok || err != nil {
		if err != nil {
			c.Send("Ошибка создания заявки! " + err.Error())
			return
			//TODO:
		} else if !ok {
			c.Send("Парк не оплатил сервис")
			return
		}
	}

	offer.FleetID = fleetID
	a.stater.ResetState(int(c.UserID))

	offerID, err := a.client.OffersCreate(context.TODO(), offer)
	if err != nil {
		handleError(c, err)
		return
	}
	offer.ID = offerID

	text, _ := renderOffer(offer, fleetUser, agent, driver, nil)

	a.sendImages(offer.FleetID, driver.Images)
	a.b.Send("Новая заявка!\n"+text, tamework.ToChat(offer.FleetID), tamework.Markdown(), tamework.WithKeyboard(tamework.NewKeyboard(nil).AddCallbackButton("Отклонить", "rejectOffer:"+strconv.Itoa(offer.ID)).AddCallbackButton("Подтвердить", "allowDriver:"+strconv.Itoa(offer.DriverID))))

	c.Send("Заявка отправлена!")
}

func (a *App) HandleAddDocuments(c *tamework.Context) {
	driverID := strToInt(c.Text)

	driver, _ := a.client.DriversByID(context.TODO(), driverID)

	user := &Driver{ID: driverID, Images: driver.Images}
	a.stater.SetState(int(c.UserID), StateUploadDriverDocuments, user)
	c.Keyboard.AddReplyButton("Отмена")
	c.Send("Загрузите фотографии с документами.")
}

// HandleSubmitFleet handle fleet submit
func (a *App) HandleSubmitFleet(c *tamework.Context) {
	userID, _ := strconv.Atoi(c.Text)
	user := &User{
		ID:   userID,
		Role: UserRoleFleet,
	}
	a.stater.SetState(int(c.UserID), StateInputFleetName, user)
	c.Send("Введите название парка")
}

func (a *App) HandleAgentName(user *User, c *tamework.Context) {
	user.Name = c.Text
	a.stater.SetState(int(c.UserID), StateInputCity, user)
	c.Send("Введите город:")
}

func (a *App) HandleFleetName(user *User, c *tamework.Context) {
	user.Name = c.Text
	a.stater.SetState(int(c.UserID), StateInputCity, user)
	c.Send("Введите город:")
}

func (a *App) HandleAgentCity(user *User, c *tamework.Context) {
	ctx := context.Background()
	cities, err := a.client.UsersCities(ctx)
	if err != nil {
		handleError(c, err)
		return
	}
	for _, city := range cities {
		if strings.ToLower(city.Title) == strings.ToLower(c.Text) {
			user.CityID = city.ID
		}
	}
	a.stater.SetState(int(c.UserID), StateInputPhone, user)
	c.Send("Введите номер телефона:")
}

func strToInt(i string) int {
	ii, _ := strconv.Atoi(i)
	return ii
}

func strToBool(i string) bool {
	var words = []string{"да", "бренд", "есть"}
	for _, w := range words {
		if strings.TrimSpace(strings.ToLower(i)) == w {
			return true
		}
	}
	return false
}

func parseCarData(text string) (Car, error) {
	return Car{
		Text: text,
	}, nil
}

func (a *App) HandleCreateCar(c *tamework.Context) {
	a.stater.ResetState(int(c.UserID))
	car, err := parseCarData(c.Text)
	if err != nil {
		c.Send(err.Error())
		return
	}
	car.FleetUserID = int(c.UserID)
	_, err = a.client.CarsCreate(context.TODO(), car)
	if err != nil {
		c.Send(err.Error())
		return
	}

	c.Send("Автомобиль создан!")
}

func parseDriverData(text string) (Driver, error) {
	arr := strings.Split(text, "\n")
	if len(arr) != 2 {
		return Driver{}, fmt.Errorf("Не правильный формат, должно быть 2 строки")
	}
	return Driver{
		Name:  arr[0],
		Phone: arr[1],
	}, nil
}

func (a *App) saveDriverDocs(c *tamework.Context) {
	var driver Driver
	a.stater.GetStateWithPayload(int(c.UserID), &driver)

	dbDriver, err := a.client.DriversByID(context.TODO(), driver.ID)
	if err != nil {
		c.Send(err.Error())
		return
	}
	dbDriver.Images = driver.Images

	err = a.client.DriversUpdate(context.TODO(), dbDriver)
	if err != nil {
		c.Send(err.Error())
		return
	}
	c.Keyboard = _agentMainKeyboard
	c.Send("Документы сохранены")

	c.Text = strconv.Itoa(dbDriver.ID)
	a.HandleDriver(c)
}

func getFileID(upd tamework.Update) (string, bool) {

	if upd.Message == nil {
		return "", false
	}

	//TODO: if need file
	// if upd.Message.Document != nil {
	// 	return upd.Message.Document.FileID, true
	// }

	if upd.Message.Photo == nil {
		return "", false
	}

	var max int
	var maxID string
	for _, s := range *upd.Message.Photo {
		if s.Width > max {
			max = s.Width
			maxID = s.FileID
		}
	}
	return maxID, true
}

func (a *App) HandleUploadDriverDocuments(c *tamework.Context) {
	var driver Driver
	a.stater.GetStateWithPayload(int(c.UserID), &driver)

	fileID, has := getFileID(c.Update())
	if !has {
		c.Send("Ошибка! Документ может быть только изображением!")
		return
	}

	driver.Images = append(driver.Images, fileID)
	a.stater.SetState(int(c.UserID), StateUploadDriverDocuments, &driver)
	log.Println(driver.Images)
	c.Keyboard.Reset().AddReplyButton(buttonCancel).AddReplyButton(buttonSubmit)
	c.Send("Фото загружено")
}

func updateContentType(u tamework.Update) string {
	return fmt.Sprint(u.Type())
}

func (a *App) HandleCreateDriver(c *tamework.Context) {
	driver, err := parseDriverData(c.Text)
	if err != nil {
		c.Keyboard.AddCallbackButton("Добавить водителя", "addDriver")
		c.Send(err.Error())
		return
	}
	a.stater.ResetState(int(c.UserID))
	driver.AgentID = int(c.UserID)
	id, err := a.client.DriversCreate(context.TODO(), driver)
	if err != nil {
		c.Send(err.Error())
		return
	}

	c.Text = strconv.Itoa(id)
	a.HandleDriver(c)
}

func (a *App) HandleAgentPhone(user *User, c *tamework.Context) {
	user.Phone = c.Text

	err := a.client.UsersUpdate(context.TODO(), *user)
	if err != nil {
		c.Send("Ошибка сохраниения пользователя")
		return
	}

	a.stater.ResetState(int(c.UserID))
	a.b.Send("Ваша заявка успешно одобрена!", tamework.ToChat(user.ID))

	if user.Role == UserRoleAgent {
		c.Send("Агент успешно создан!")
	} else if user.Role == UserRoleFleet {
		c.Send("Парк успешно создан!")
		a.b.Send("Для получения заявок необходимо пополнить баланс!", tamework.ToChat(user.ID))
	}
}

func generatePaymentLink(qiwiAccount string, amount int, comment string) string {
	var params = url.Values{}
	params.Set("amountInteger", strconv.Itoa(amount))
	params.Set("amountFraction", strconv.Itoa(0))
	params.Set("extra['account']", qiwiAccount)
	params.Set("extra['comment']", comment)
	params.Set("blocked[0]", "account")
	params.Set("blocked[1]", "comment")
	return "https://qiwi.com/payment/form/99?" + params.Encode()
}

func detectTxUserID(comment string) int {
	i, err := strconv.Atoi(comment)
	if err != nil {
		return 0
	}
	return i - GlobalSalt
}

func (a *App) processIncomingPayment(userID, amount int) error {
	log.Printf("new incoming tx! user=%d amount=%d", userID, amount)
	ctx := context.TODO()
	user, err := a.client.UsersByID(ctx, userID)
	if err != nil {
		log.Printf("incoming tx without receipent user=%d err=%s", userID, err)
		return nil
	}
	if user.ID == 0 {
		log.Printf("incoming tx without receipent user=%d", userID)
		return nil
	}

	_, err = a.client.WalletCreateTransaction(context.TODO(), Transaction{
		Amount: float64(amount),
		Status: TransactionStatusDone,
		UserID: user.ID,
	})
	if err != nil {
		log.Printf("create tx err=%s", err)
		return nil
	}

	err = a.client.UsersUpdate(ctx, user)
	if err != nil {
		return err
	}

	a.b.Send(fmt.Sprintf("Ваш баланс пополнен на %d руб!", amount), tamework.ToChat(user.ID))

	return nil
}

// func (a *App) checkTransactions() error {
// 	qiwiWallet, qiwiToken, err := a.qiwiCredentials()
// 	if err != nil {
// 		log.Printf("err get qiwi creds err=%s", err)
// 		return err
// 	}
// 	log.Println("check txs")
// 	qw := qiwi.New(qiwiToken, qiwi.Wallet(qiwiWallet))

// 	return nil
// }

func (a *App) runNotifier() {
	for ticker := time.NewTicker(10 * time.Second); ; <-ticker.C {
		err := a.notifyPayments()
		if err != nil {
			var nerr *net.OpError
			if !errors.As(err, &nerr) {
				a.b.Send(fmt.Sprintf("Ошибка уведомлятора!\nerr=`%s`", err), tamework.ToChat(102710272))
			}
		}
	}
}

func (a *App) notifyPayments() error {
	transactions, err := a.client.PaymentsTransactionsForNotification(context.TODO())
	if err != nil {
		return err
	}
	for _, tx := range transactions {
		err = a.client.PaymentsSetTransactionNotificationSent(context.TODO(), tx.ID)
		if err != nil {
			return err
		}
		if tx.Amount > 0 {
			a.b.Send("Баланс пополнен!", tamework.ToChat(tx.UserID))
		} else {
			a.b.Send(fmt.Sprintf("Списание: %d₽, переведено на киви.", int(tx.Amount)), tamework.ToChat(tx.UserID))
			ctx := context.Background()
			balance, err := a.getUserBalance(ctx, tx.UserID)
			if err != nil {
				return err
			}
			if balance < -299 {
				a.b.Send("Входящие заявки недоступны. Для продолжения работы с сервисом пополните Ваш баланс.", tamework.ToChat(tx.UserID))
			}
		}
	}
	return nil
}

func (a *App) runBalanceSyncer() {
	for ticker := time.NewTicker(5 * time.Minute); ; <-ticker.C {
		err := a.syncBalance()
		if err != nil {
			a.b.Send(fmt.Sprintf("Ошибка синхронизации qiwi-кошельков!\n`%s`", err), tamework.ToChat(102710272))
		}
	}
}

// TODO: move to server
func (a *App) syncBalance() error {
	log.Printf("start sync balance")
	list, err := a.client.PaymentsList(context.TODO())
	if err != nil {
		return fmt.Errorf("payments list: %w", err)
	}
	log.Printf("sync balance for %d wallets", len(list))
	for _, l := range list {
		if l.Status != PaymentSystemStatusActive {
			log.Printf("skip not active wallet")
			continue
		}
		arr := strings.Split(l.Setting, ":")
		if len(arr) != 2 {
			err = fmt.Errorf("bad wallet setting: %s", l.Setting)
			return err
		}
		account, token := arr[0], arr[1]

		qw := qiwi.New(token, qiwi.Wallet(account))
		br, err := qw.Balance.Current(context.TODO())
		if err != nil {
			// отключаем кошелек
			l.Status = PaymentSystemStatusDisabled
			l.Balance = 0
			updErr := a.client.PaymentsUpdate(context.TODO(), l)
			if err != nil {
				return fmt.Errorf("payments update: %w", updErr)
			}
			return fmt.Errorf("current balance: %w", err)
		}

		var b float64
		for _, acc := range br.Accounts {
			if !acc.HasBalance {
				continue
			}
			b += acc.Balance.Amount
		}

		// save balance

		if l.Balance != b {
			a.b.Send(fmt.Sprintf("Баланс кошелька поменялся!\nКошелек: %s\nБыло:%.2f\nСтало:%.2f", account, l.Balance, b), tamework.ToChat(102710272))
			l.Balance = b
			err = a.client.PaymentsUpdate(context.TODO(), l)
			if err != nil {
				return fmt.Errorf("payments update 2: %w", err)
			}
		}

		transactions, err := qw.Payments.History(context.TODO(), 10)
		if err != nil {
			return fmt.Errorf("payments history: %w", err)
		}

		for _, tx := range transactions.Data {
			_, processed := processedTxs[tx.TrmTxnID]
			if userID := detectTxUserID(tx.Comment); userID != 0 && !processed && tx.Date.After(StartTime) && tx.Type == "IN" && tx.Status == "SUCCESS" {
				processedTxs[tx.TrmTxnID] = struct{}{}
				err := a.processIncomingPayment(userID, int(tx.Sum.Amount))
				if err != nil {
					return fmt.Errorf("process incoming payment: %w", err)
				}
			}
		}
	}
	return nil
}

func handleError(c *tamework.Context, err error) {
	id := xid.New().String()
	log.Printf("error id=%s err=%s text=%s", id, err, c.Text)
	c.Markdown(fmt.Sprintf("Ошибка `%s`", id))
}
