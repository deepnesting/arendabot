module gitlab.com/deepnesting/arendabot

go 1.13

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/rs/xid v1.2.1
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/zhuharev/qiwi v1.0.0
	github.com/zhuharev/tamework v0.1.0
	golang.org/x/sys v0.0.0-20191007154456-ef33b2fb2c41 // indirect
)
