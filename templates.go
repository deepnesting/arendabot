package main

import (
	"bytes"
	"text/template"
)

const offersTmpl = `Заявки
{{ range .offers }}{{ .ID }} Статус: {{ .StatusText }} /offer{{.ID}}
Водитель: {{ ($.drivers.ByID .DriverID).Name }} ({{ ($.drivers.ByID .DriverID).Phone }})
Дата: {{ .CreatedAt.Format "02.01.2006"}}

{{ end }}`

type Drivers []Driver

func (ds Drivers) ByID(id int) Driver {
	for _, d := range ds {
		if d.ID == id {
			return d
		}
	}
	return Driver{}
}

func renderOffers(offers []Offer, drivers []Driver) (string, error) {
	return execTemplate(offersTmpl, map[string]interface{}{"offers": offers, "drivers": Drivers(drivers)})
}

const offerTmpl = `

Агент: {{ .agent.Name }} (@{{ .agent.Username }})
Водитель: {{ .driver.Name }} (` + "`{{ .driver.Phone }}`" + `)
Статус: {{ .offer.StatusText }}
`

//{{ range .shifts }}
//Смена {{ .ID }} /shift{{ .ID }}
//{{ end }}

func renderOffer(offer Offer, fleet, agent User, driver Driver, shifts []Shift) (string, error) {
	return execTemplate(offerTmpl, map[string]interface{}{"offer": offer, "fleet": fleet, "agent": agent, "driver": driver, "shifts": shifts})
}

const carsTmpl = `Машины
{{ range .cars }}/car{{ .ID }} {{ . }}
{{ end }}`

func renderCars(cars []Car) (string, error) {
	return execTemplate(carsTmpl, map[string]interface{}{"cars": cars})
}

const driversTmpl = `Водители
{{ range .drivers }}/driver{{ .ID }} {{ .Name }} {{ if not .Images }}*🚫 документы не загружены*{{else}}✅ документы загружены{{end}}
{{ end }}`

func renderDrivers(drivers []Driver) (string, error) {
	return execTemplate(driversTmpl, map[string]interface{}{"drivers": drivers})
}

const fleetsTmpl = `Парки
{{ range .fleets }}ID: *{{ .ID }}* *{{ .Name }}*
{{ end }}`

func renderFleets(users []User) (string, error) {
	return execTemplate(fleetsTmpl, map[string]interface{}{"fleets": users})
}

const fleetTmpl = `Вы отправляете водителя в парк {{ .fleet.Name }}
Телефон: {{ .fleet.Phone }}
Бонус: {{ .fleet.AgentReward }}₽`

func renderFleet(fleet User) (string, error) {
	return execTemplate(fleetTmpl, map[string]interface{}{"fleet": fleet})
}

const driverTmpl = `Водитель /driver{{ .driver.ID }}
Имя: * {{ .driver.Name }}*
Телефон: ` + "`{{ .driver.Phone }}`" + `
Документы: {{ if not .driver.Images }}*🚫 не загружены*{{else}}✅ документы загружены{{end}}`

func renderDriver(driver Driver) (string, error) {
	return execTemplate(driverTmpl, map[string]interface{}{"driver": driver})
}

const shiftTmpl = `Смена {{ .shift.ID }}
Заявка /offer{{ .offer.ID }}
Водитель /driver{{ .offer.DriverID }}
Дата {{ .shift.CreatedAt.Format "02.01.2006" }}
Статус: {{ .shift.StatusText }}
`

func renderShift(shift Shift, offer Offer) (string, error) {
	return execTemplate(shiftTmpl, map[string]interface{}{"shift": shift, "offer": offer})
}

func execTemplate(tmpl string, data map[string]interface{}) (string, error) {
	tpl, err := template.New("").Funcs(template.FuncMap{}).Parse(tmpl)
	if err != nil {
		return "", err
	}
	var buf bytes.Buffer
	err = tpl.Execute(&buf, data)
	return buf.String(), err
}
