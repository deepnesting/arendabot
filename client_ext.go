package main

import "strconv"

func (r UserRole) IsActive() bool {
	return r == UserRoleAgent || r == UserRoleFleet || r == UserRoleAdmin
}

func (s Shift) StatusText() string {
	status, ok := map[ShiftStatus]string{
		ShiftStatusActive:    "🕑 В работе", // ⏺▶️⬜️◻️
		ShiftStatusCancelled: "🚫 Отменена",
		ShiftStatusDone:      "✅ Завершена",
	}[s.Status]
	if !ok {
		return "🕑 В работе: " + strconv.Itoa(s.Status.Int())
	}
	return status
}

func (o Offer) StatusText() string {
	s, ok := map[OfferStatus]string{
		OfferStatusApproved: "Одобрена",
		OfferStatusNew:      "Ожидает решения парка",
		OfferStatusRejected: "Отклонена",
		OfferStatusWorking:  "Принят в парк",
	}[o.Status]
	if !ok {
		return "Ожидает решения парка"
	}
	return s
}

func (o Offer) IsEnded() bool {
	return o.Status == OfferStatusCancelByAgent ||
		o.Status == OfferStatusCancelByFleet ||
		o.Status == OfferStatusDone ||
		o.Status == OfferStatusRejected
}
