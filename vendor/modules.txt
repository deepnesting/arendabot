# github.com/fatih/color v1.7.0
github.com/fatih/color
# github.com/go-macaron/inject v0.0.0-20160627170012-d8a0b8677191
github.com/go-macaron/inject
# github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
github.com/go-telegram-bot-api/telegram-bot-api
# github.com/mattn/go-colorable v0.1.4
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.9
github.com/mattn/go-isatty
# github.com/technoweenie/multipartstreamer v1.0.1
github.com/technoweenie/multipartstreamer
# github.com/zhuharev/qiwi v1.0.0
github.com/zhuharev/qiwi
# github.com/zhuharev/tamework v0.1.0
github.com/zhuharev/tamework
# golang.org/x/sys v0.0.0-20191007154456-ef33b2fb2c41
golang.org/x/sys/unix
